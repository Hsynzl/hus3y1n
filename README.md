<img src="https://media.giphy.com/media/jx5MJyZqAFsLS/giphy.gif"
align="right" width="400" height="250">
### Hello friend, I'm Hüseyin 

## PHP Devoloper | Pythonista
<font color="brown"><> Coffee is my life </> </font>

[<img  width="22" src="https://unpkg.com/simple-icons@v4/icons/twitter.svg" align="left" />][twitter]
 
[<img  width="22" src="https://unpkg.com/simple-icons@v4/icons/instagram.svg"  align="left"/>][İnstagram]
 
[<img width="22" src="https://simpleicons.org/icons/linkedin.svg" align="left" />][Linkedln]

<br/>
<br/>

### Languages and Frameworks 
<hr>
<img width="28" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/html/html.png"><img width="28" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/css/css.png"><img width="28" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png"><img width="28" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/php/php.png"><img width="28" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png"><img width="28" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/java/java.png"><img width="28" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/react/react.png"><img width="28" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/cpp/cpp.png">
<hr>
##  Blog posts
<!-- BLOG-POST-LIST:START -->
  Soon...
<!-- BLOG-POST-LIST:END -->
 <hr>

 <details>
 <summary>:bulb: Github Stats :</summary>
 <img src="https://github-readme-stats.vercel.app/api?username=hus3y1n&theme=tokyonight">
 
 </details>
 <details>
 <br>
 <summary>:bulb: Most Used Languages<br></summary>
 <img src="https://github-readme-stats.vercel.app/api/top-langs/?username=hus3y1n&theme=tokyonight">
 </details>

[twitter]: https://twitter.com/_hozlu_
[İnstagram]: https://www.instagram.com/hus3y1n_zl_/
[Linkedln]: www.linkedin.com/in/hüseyin-özlü


